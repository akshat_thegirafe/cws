//STELLERNAV JS STARTS  
  jQuery('#mainnav').stellarNav({
    theme     : 'plain', 
    breakpoint: 991, 
    mobileMode: false,
    phoneBtn: false,
    locationBtn: false, 
    sticky     : false, 
    openingSpeed: 250, 
    closingDelay: 250, 
    position: 'right', 
     menuLabel: '',
    showArrows: true, 
    closeBtn     : false, 
    scrollbarFix: false
  });
//STELLERNAV JS ENDS